# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

import numpy as np
from apps.home.forms import DocumentForm
from apps.home.models import Document
from apps.home.utils.ranker import Ranker
from apps.home.utils.utility import SingletonRanker
from django import template
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.views.generic import TemplateView
from django.core.paginator import Paginator


class IndexView(TemplateView):
    template_name = "home/index.html"

    def get_context_data(self, **kwargs):
        ranker: SingletonRanker = SingletonRanker()
        context = super().get_context_data(**kwargs)
        query = self.request.GET.get('query')
        if query is not None:
            res = []
            query_hash = hash(query)
            context["query"] = query
            # check cache
            relevant_docs = cache.get(query_hash)
            if relevant_docs is None:
                relevant_docs = ranker.relevant_docs(query=query)
                cache.set(query_hash, relevant_docs, 60)
            else:
                cache.touch(query_hash, 60)
            paginator = Paginator(relevant_docs, 10)
            page_number = self.request.GET.get('page')
            page_obj = paginator.get_page(page_number)
            context["page_obj"] = page_obj
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class DocumentsView(TemplateView):
    template_name = "home/documents.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['docs'] = Document.objects.all()
        form = DocumentForm()
        context['form'] = form
        return context

    def post(self, request, *args, **kwargs):
        form = DocumentForm(data=request.POST)
        context = self.get_context_data()
        if form.is_valid():
            form.save()
        else:
            context['form'] = form
        return self.render_to_response(context)


def index(request):
    request.GET
    context = {'segment': 'index'}

    html_template = loader.get_template('')
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]

        if load_template == 'admin':
            return HttpResponseRedirect(reverse('admin:index'))
        context['segment'] = load_template

        html_template = loader.get_template('home/' + load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('home/page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:
        html_template = loader.get_template('home/page-500.html')
        return HttpResponse(html_template.render(context, request))

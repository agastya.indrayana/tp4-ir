# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.db import models
from django.contrib.auth.models import User


class Document(models.Model):
    doc_id = models.CharField(blank=False, unique=True, max_length=50)
    content = models.TextField(blank=False)

from django.forms import ModelForm
from apps.home.models import Document

class DocumentForm(ModelForm):
    class Meta:
        model = Document
        fields = ['doc_id', 'content']
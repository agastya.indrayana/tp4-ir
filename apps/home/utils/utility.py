
import random

import lightgbm
import numpy as np
from apps.home.models import Document
from apps.home.utils.ranker import Ranker
from django.core.cache import cache
from django.db import models
from gensim.corpora import Dictionary
from gensim.models import LsiModel, TfidfModel
from lightgbm import LGBMRanker
from scipy.spatial.distance import cosine
from tqdm import tqdm

NUM_NEGATIVES = 1


class SingletonRanker(object):
    NUM_NEGATIVES = 1
    NUM_LATENT_TOPICS = 200
    documents = {}
    queries = {}
    dictionary = None
    model = None
    ranker: LGBMRanker = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(SingletonRanker, cls).__new__(cls)
            cls.instance._load_documents()
            cls.instance._build_lsi_model()
            cls.instance._train()
        return cls.instance

    def _load_documents(self):
        with open("dev/nfcorpus/train.docs") as file:
            for line in tqdm(file):
                doc_id, content = line.split("\t")
                self.documents[doc_id] = content.split()
        with open("dev/nfcorpus/train.vid-desc.queries", encoding="utf8") as file:
            for line in tqdm(file):
                q_id, content = line.split("\t")
                self.queries[q_id] = content.split()

    def _build_lsi_model(self):
        self.dictionary = Dictionary()
        bow_corpus = [self.dictionary.doc2bow(doc, allow_update=True)
                      for doc in self.documents.values()]
        self.model = LsiModel(bow_corpus, num_topics=self.NUM_LATENT_TOPICS)

    def _train(self):
        q_docs_rel = {}  # grouping by q_id terlebih dahulu
        with open("dev/nfcorpus/train.3-2-1.qrel", encoding="utf8") as file:
            for line in tqdm(file):
                q_id, _, doc_id, rel = line.split("\t")
                if (q_id in self.queries) and (doc_id in self.documents):
                    if q_id not in q_docs_rel:
                        q_docs_rel[q_id] = []
                    q_docs_rel[q_id].append((doc_id, int(rel)))
        group_qid_count = []
        dataset = []
        for q_id in q_docs_rel:
            docs_rels = q_docs_rel[q_id]
            group_qid_count.append(len(docs_rels) + NUM_NEGATIVES)
            for doc_id, rel in docs_rels:
                dataset.append(
                    (self.queries[q_id], self.documents[doc_id], rel))
            # tambahkan satu negative (random sampling saja dari documents)
            dataset.append((self.queries[q_id], random.choice(
                list(self.documents.values())), 0))
        X = []
        Y = []
        for (query, doc, rel) in tqdm(dataset):
            X.append(self._features(query, doc))
            Y.append(rel)
        # ubah X dan Y ke format numpy array
        X = np.array(X)
        Y = np.array(Y)
        self.ranker = lightgbm.LGBMRanker(
            objective="lambdarank",
            boosting_type="gbdt",
            n_estimators=100,
            importance_type="gain",
            metric="ndcg",
            num_leaves=40,
            learning_rate=0.02,
            max_depth=-1)
        # di contoh kali ini, kita tidak menggunakan validation set
        # jika ada yang ingin menggunakan validation set, silakan saja
        self.ranker.fit(X, Y,
                        group=group_qid_count,
                        verbose=10)

    def _vector_rep(self, text):
        rep = [topic_value for (_, topic_value)
               in self.model[self.dictionary.doc2bow(text)]]
        return rep if len(rep) == self.NUM_LATENT_TOPICS else [0.] * self.NUM_LATENT_TOPICS

    def _features(self, query, doc):
        v_q = self._vector_rep(query)
        v_d = self._vector_rep(doc)
        q = set(query)
        d = set(doc)
        cosine_dist = cosine(v_q, v_d)
        jaccard = len(q & d) / len(q | d)
        return v_q + v_d + [jaccard] + [cosine_dist]

    def _generate_qd_pairs(self, query: str, documents):
        X = []
        for doc_id, content in documents:
            X.append(self._features(query.split(), content.split()))
        return np.array(X)

    def _get_available_docs(self):
        documents = cache.get("available_docs")
        if documents is None:
            documents = Document.objects.values_list('doc_id', 'content')
            cache.set('available_docs', documents, 120)
        return documents

    def relevant_docs(self, query):
        documents = Document.objects.values_list('doc_id', 'content')
        qd_pairs = self._generate_qd_pairs(query=query, documents=documents)
        print(qd_pairs)
        # hitung scores
        scores = self.ranker.predict(qd_pairs)
        did_scores = [x for x in zip(
            [did for (did, _) in documents], [content for (_, content) in documents], scores)]
        sorted_did_scores = sorted(
            did_scores, key=lambda tup: tup[2], reverse=True)
        return sorted_did_scores


ranker = SingletonRanker()
query = "how much cancer risk can be avoided through lifestyle change ?"
